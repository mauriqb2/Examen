package com.ucbcba.post.Entities;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Set;

@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Size(min=1, max=20,message="the title must be between 1 and 20 characters")
    private String title;

    @NotNull
    @Size(min=1, max=100,message="the text must be between 1 and 100 characters")
    private String text;

    @NotNull
    private Integer likes;

    private String url;

    @NotNull
    private Boolean showPost;

    @OneToMany(mappedBy = "post")
    private Set<Comment> comments;

    @NotNull
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    @NotNull
    @ManyToOne
    @JoinColumn(name="category_id")
    private Category category;

    @NotNull(message = "Can not be empty")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date date;

    public Post(){
     likes=0;
     date=Date.valueOf(LocalDate.now());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setDate(LocalDate date) {
        this.date = Date.valueOf(date);
    }

    public LocalDate getDate() {
        return date.toLocalDate();
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setShowPost(Boolean showPost) {
        this.showPost = showPost;
    }

    public Boolean getShowPost() {
        return showPost;
    }
}
