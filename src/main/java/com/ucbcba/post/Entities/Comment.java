package com.ucbcba.post.Entities;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.time.LocalDate;

@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Size(min=1, max=50,message="the text must be between 1 and 50 characters")
    private String text;

    @NotNull
    private Integer likes;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date date;

    @NotNull
    @ManyToOne
    @JoinColumn(name="post_id")
    private Post post;

    @NotNull
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    public Comment(){
        likes=0;
        setDate(LocalDate.now());
    }

    public Comment(Post post){
        this.post=post;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Post getPost() {
        return post;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setDate(LocalDate date) {
        this.date = Date.valueOf(date);
    }

    public Date getDate() {
        return date;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

}
