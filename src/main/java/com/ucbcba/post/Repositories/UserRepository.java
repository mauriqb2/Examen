package com.ucbcba.post.Repositories;

import com.ucbcba.post.Entities.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends CrudRepository<User,Integer>{
}
