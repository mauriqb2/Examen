package com.ucbcba.post.Repositories;

import com.ucbcba.post.Entities.Category;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CategoryRepository extends CrudRepository<Category,Integer>{
}
