package com.ucbcba.post.Repositories;

import com.ucbcba.post.Entities.Comment;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CommentRepository extends CrudRepository<Comment,Integer>{
}
