package com.ucbcba.post.Services;

import com.ucbcba.post.Entities.Comment;

public interface CommentService {

    Iterable<Comment> listAllComments();

    Comment getCommentById(Integer id);

    Comment saveComment(Comment comment);

    void deleteComment(Integer id);
}
