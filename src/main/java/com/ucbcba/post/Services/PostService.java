package com.ucbcba.post.Services;

import com.ucbcba.post.Entities.Post;

public interface PostService {

    Iterable<Post> listAllPosts();

    Post getPostById(Integer id);

    Post savePost(Post post);

    void deletePost(Integer id);

}
