package com.ucbcba.post.Services;

import com.ucbcba.post.Entities.Category;

public interface CategoryService {

    Iterable<Category> ListAllCategories();

    Category getCategoryById(Integer id);

    Category saveCategory(Category category);

    void deleteCategory(Integer id);
}
